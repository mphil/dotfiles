- **Emacs**
I use it mostly for programming and org-mode. Humble configuration, with few
very cool packages like magit, ace-jump, avy, ace-window. Indespensable for 
functional programming. And I got used to ansi-term and it's windowing system.
Works much more consistent that tmux.

- **iTerm2**
Using it since 3.10beta3, very satisfied, goes along well with everything,
not laggs, plus dark top bar looks awesome.

- **Neovim**
I am using Neovim for year and a half, it is my goto terminal editor. I just
don't like firing up Emacs through terminal for various reasons, and it takes
a lot to load. So I configured minimal rc and it does the job for my needs.

- **tmux**
Configured bottom bar to show windows, open panes, CPU threads, free space,
battery and date & time. Few key bindings here and there, and little color
adjustments. Nothing special.

- **zsh**
oh-my-zsh, prezto and pure felt like too much, often slow compared to naked
zsh with simple prompt (I like "suse" the most). Yeah, I kinda miss that 
git status in prompt, but it oftens makes prompt heavy. So I just have to
type `git status` a little bit more often now, but considering the speed
I think it is worth it.
